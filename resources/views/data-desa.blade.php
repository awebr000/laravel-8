<!DOCTYPE html>
 <html lang="en">

 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">

 	<title> Smart Desa | Kec. Benjeng Gresik</title>

 	<!--Favicon-->
 	<link rel="icon" href="{{asset('img/favicon.png')}}" type="image/jpg" />
 	<!-- Bootstrap CSS -->
 	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
 	<!-- Font Awesome CSS-->
 	<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
 	<!-- Line Awesome CSS -->
 	<link href="{{asset('css/line-awesome.min.css')}}" rel="stylesheet">
 	<!-- Animate CSS-->
 	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
 	<!-- Flaticon CSS -->
 	<link href="{{asset('css/flaticon.css')}}" rel="stylesheet">
 	<!-- Owl Carousel CSS -->
 	<link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
 	<!-- Style CSS -->
 	<link href="{{asset('css/style.css')}}" rel="stylesheet">
 	<!-- Responsive CSS -->
 	<link href="{{asset('css/responsive.css')}}" rel="stylesheet">
	 <link href="{{asset('css/new.css')}}" rel="stylesheet">
 	<!-- jquery -->
 	<script src="{{asset('js/jquery-1.12.4.min.js')}}"></script>

    <style>
    
    </style>
 </head>

 <body>

 	<!-- Pre-Loader -->
 	<div id="loader">
 		<div class="loading">
 			<div class="spinner">
 				<div class="double-bounce1"></div>
 				<div class="double-bounce2"></div>
 			</div>
 		</div>
 	</div>

 	<!-- Header Top Area -->
    <!-- Header Area -->

    <!-- TODO: CHANGE TO CSS FILE -->
    <!-- NORMAL NAV -->
    <header class="header-area" id="above-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-3">
 							<div class="logo">
 								<a class="navbar-brand" href="/">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng</span>
                                            </br>
                                            <span style="font-size: 12px">Kab. Gresik | Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>

 						<div class="col-lg-9 col-md-6" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
 									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>
 									<div class="collapse navbar-collapse justify-content-center">
 										<ul class="navbar-nav m-auto">
 											<li class="nav-item">
 												<a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
                                                     <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                    <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 													
 												</ul>
 											</li>
 											<li class="nav-item active">
 												<a class="nav-link active" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>

 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                     <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/pembuatan-kk">Pelayanan Publik</a>
                                                <ul class="sub-menu">
													<li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn">Login</a>
                                            </li>
                                            <li style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
 						<div class="col-lg-2">
 							
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
    </header>
     <!-- END OF NORMAL NAV  -->
    
    <!-- NAV UNDER 1200 -->
     <header class="header-area" id="nav-under-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-12">
 							<div class="logo">
 								<a class="navbar-brand" href="/">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng <br> Kab. Gresik, Prov. Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
                                    <button style="right: -1em" class="navbar-toggler search-btn" type="button" data-toggle="collapse">
                                         <i style="font-size: 30px;" class="la la-search"></i>
                                     </button>
 									<button style="right: 1em" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>

 									<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
 										<ul class="navbar-nav m-auto">
 											<li class="nav-item">
 												<a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
                                                     <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                     <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 													
 												</ul>
 											</li>
 											<li class="nav-item active">
 												<a class="nav-link active" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>

 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                     <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/pembuatan-kk">Layanan Publik</a>
                                                <ul class="sub-menu">
													<li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn" id="login-1">Login</a>
                                                <a href="#" class="main-btn" id="login-2">login</a>
                                            </li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
                    </div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
     </header>
     <!-- END NAV UNDER 1200 -->


     <div class="programe-area section-padding">
 		<div class="container">
 			<div class="row">
 				<div class="offset-lg-2 col-lg-8 text-center">
 					<div class="section-title">
 						<h2>Statistik <b>Data Desa</b></h2>
 					</div>
 				</div>
 			</div>
 			<div class="programe-wrapper">
 				<div class="row no-gutters">
 					<div class="col-lg-12 col-md-12 col-12">
 						<div class="programe-inner">
						 	<!-- <button id="delete-button">Delete Selected Shape</button> -->
							<div class="row" style="text-align: left">
								<div class="col-lg-6 col-md-12 mb-3">
                                    <canvas id="chart-1" style="height: 300px; width: 100%"></canvas>
								</div>
								<div class="col-lg-6 col-md-12">
                                    <canvas id="chart-2" style="height: 300px; width: 100%"></canvas>
								</div>
							</div>
 						</div>
 					</div>
 				</div>
             </div>

             <div class="programe-wrapper mt-5">
 				<div class="row no-gutters">
 					<div class="col-lg-12 col-md-12 col-12">
 						<div class="programe-inner">
						 	<!-- <button id="delete-button">Delete Selected Shape</button> -->
							<div class="row" style="text-align: left">
								<div class="col-lg-6 col-md-12 mb-3">
                                    <canvas id="chart-3" style="height: 300px; width: 100%"></canvas>
								</div>
								<div class="col-lg-6 col-md-12">
                                    <canvas id="chart-4" style="height: 300px; width: 100%"></canvas>
								</div>
							</div>
 						</div>
 					</div>
 				</div>
             </div>

             <div class="programe-wrapper mt-5">
 				<div class="row no-gutters">
 					<div class="col-lg-12 col-md-12 col-12">
 						<div class="programe-inner">
                            <div class="pb-3">
                                <h4 style="text-align: center; color: #3b5688;">Data Perekonomian Desa</h4>
                                <span style="color:#3b5688";>(2020 - 2021)</span>
                            </div>
							<div class="row" style="text-align: left">
								<div class="col-lg-12 col-md-12 mb-3">
                                    <canvas id="chart-5" style="height: 300px; width: 100%"></canvas>
								</div>
							</div>
 						</div>
 					</div>
 				</div>
             </div>
 		</div>
 	</div>


 	<div class="spacer">
 		<div class="spacer-bg gray-bg bg-cover">
 		</div>
     </div>
     <div class="spacer">
 		<div class="spacer-bg gray-bg bg-cover">
 		</div>
     </div>
     <div class="spacer">
 		<div class="spacer-bg gray-bg bg-cover">
 		</div>
 	</div>

 	<!-- Footer Area -->

 	<footer class="footer-area blue-bg">
 		<div class="container">
 			<div class="footer-up">
 				<div class="row">
 					<div class="col-lg-3 col-md-6 col-sm-12">
					 	<h5>Kecamatan Benjeng</h5>
 						<p>Jl. Raya Munggugianti No.8, Bengkelolor, Bulurejo, Benjeng, Kabupaten Gresik, Jawa Timur 61172</p>
 						<div class="social-area">
 							<a href=""><i class="lab la-facebook-f"></i></a>
 							<a href=""><i class="lab la-instagram"></i></a>
 							<a href=""><i class="lab la-twitter"></i></a>
 							<a href=""><i class="la la-skype"></i></a>
 						</div>
 					</div>
 					<div class="col-lg-2 offset-lg-1 col-md-6 com-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6 col-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6">
 						<div class="subscribe-form">
 							<h5>Berlangganan</h5>
 							<p>Dapatkan berita dan pengumuman terbaru</p>
 							<form action="index.html">
 								<input type="email" placeholder="Alamat Email">
 								<button class="main-btn">Mulai langganan</button>
 							</form>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</footer>

 	<!-- Footer Bottom Area -->
 	<div class="footer-bottom">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-6 col-md-6 col-sm-12">
 					<p class="copyright-line">© 2021 . All rights reserved.</p>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Scroll Top Area -->
 	<a href="#top" class="go-top"><i class="las la-angle-up"></i></a>


 	<!-- Popper JS -->
 	<script src="{{asset('js/popper.min.js')}}"></script>
 	<!-- Bootstrap JS -->
 	<script src="{{asset('js/bootstrap.min.js')}}"></script>
 	<!-- Wow JS -->
 	<script src="{{asset('js/wow.min.js')}}"></script>
 	<!-- Way Points JS -->
 	<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
 	<!-- Counter Up JS -->
 	<script src="{{asset('js/jquery.counterup.min.js')}}"></script>
 	<!-- CountDown JS -->
 	<script src="{{asset('js/jquery.countdown.js')}}"></script>
 	<!-- Owl Carousel JS -->
 	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
 	<!-- Sticky JS -->
	<script src="{{asset('js/jquery.sticky.js')}}"></script>
	<!-- CHART JS -->
	<script src="{{asset('js/chart.min.js')}}"></script>
 	<!-- Main JS -->
 	<script src="{{asset('js/main.js')}}"></script>
    <script>
        var ctx = document.getElementById('chart-1');
        var chartData = {};
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Laki-laki", "Perempuan", "total"],
                datasets: [{
                    label: "Jumlah Penduduk Berdasarkan Jenis Kelamin",
                    data: ["100", "130", "230"],
                    backgroundColor: [
                            'rgba(26, 56, 91, 0.267)',
                            'rgba(209, 162, 43, 0.500)',
                            'rgba(52, 146, 28, 0.400)'
                        ]
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        var ctx2 = document.getElementById('chart-2');
        var chartData = {};
        var myChart2 = new Chart(ctx2, {
            type: 'bar',
            data: {
                labels: ["<=1 thn", "2-10thn", "11-20thn", "21-30thn", "31-40thn", "41-50thn", "51-60thn", ">=60thn"],
                datasets: [{
                    label: "Jumlah Penduduk Berdasarkan Range Umur",
                    data: ["17", "25", "22", "44", "23", "40", "30", "23"],
                    backgroundColor: [
						'rgba(26, 56, 91, 0.267)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(255, 99, 132, 0.400)',
						'rgba(56, 255, 49, 0.500)',
						'rgba(167, 43, 184, 0.500)',
						'rgba(49, 230, 214, 0.500)',
						'rgba(52, 146, 28, 0.400)',
						'rgba(99, 99, 99, 0.4)',
                        ]
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        var ctx3 = document.getElementById('chart-3');
        var chartData = {};
        var myChart3 = new Chart(ctx3, {
            type: 'pie',
            data: {
                labels: ["Tidak Bekerja", "Pensiunan", "Pelajar/Mahasiswa", "Pegawai", "Pengusaha", "Pegawai Negeri"],
                datasets: [{
                    label: "Perbandingan penduduk berdasarkan pekerjaan",
                    data: ["57", "5", "82", "23", "32", "60"],
                    backgroundColor: [
                            'rgba(26, 56, 91, 0.267)',
                            'rgba(209, 162, 43, 0.500)',
                            'rgba(255, 99, 132, 0.400)',
                            'rgba(56, 255, 49, 0.500)',
                            'rgba(167, 43, 184, 0.500)',
                            'rgba(49, 230, 214, 0.500)',
                        ]
                }]
            }
        });

        var ctx4 = document.getElementById('chart-4');
        var chartData = {};
        var myChart4 = new Chart(ctx4, {
            type: 'doughnut',
            data: {
                labels: ["TK/KB", "SD Sederajat", "SMP Sederajat", "SMA Sederajat", "Diplomat", "S1 Sederajat", "S2 Sederajat"],
                datasets: [{
                    label: "Data penduduk berdasarkan pendidikan yang ditempuh",
                    data: ["12", "35", "15", "66", "10", "24", "3"],
                    backgroundColor: [
						'rgba(26, 56, 91, 0.267)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(255, 99, 132, 0.400)',
						'rgba(56, 255, 49, 0.500)',
						'rgba(167, 43, 184, 0.500)',
						'rgba(49, 230, 214, 0.500)',
						'rgba(52, 146, 28, 0.400)',
						'rgba(52, 146, 28, 0.400)',
                        ]
                }]
            }
        });

        var ctx5 = document.getElementById('chart-5');
        var myChart5 = new Chart(ctx5, {
            type: 'bar',
            data: {
                labels: ["Kuartal 1", "Kuartal 2", "Kuartal 3", "Kuartal 4"],
                datasets: [
                    {
                        label: "Pemasukan",
                        data: ["1000000", "1500000", "900000", "3000000"],
                        backgroundColor: 'rgba(52, 146, 28, 0.400)'
                    },
                    {
                        label: "Pengeluaran",
                        data: ["700000", "1000000", "500000", "2000000"],
                        backgroundColor: 'rgba(209, 162, 43, 0.500)',
                    },
                    {
                        label: "Anggaran",
                        data: ["5000000", "5000000", "4000000", "6000000"],
                        backgroundColor: 'rgba(26, 56, 91, 0.267)',
                    }
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            display: false,
                        },
                    }]
                },
                animation: {
                    // onComplete: function () {
                    //     var chartInstance = this.chart,
                    //     ctx = chartInstance.ctx;
                    //     ctx.textAlign = 'center';
                    //     ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    //     ctx.textBaseline = 'bottom';
                    //     // Loop through each data in the datasets
                    //     this.data.datasets.forEach(function (dataset, i) {
                    //         var meta = chartInstance.controller.getDatasetMeta(i);
                    //         meta.data.forEach(function (bar, index) {
                    //             var data = dataset.data[index];
                    //             ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    //         });
                    //     });
                    // }
                }
            },
        });
    </script>
 </body>

 </html>
