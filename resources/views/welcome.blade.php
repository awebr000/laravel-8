<!DOCTYPE html>
 <html lang="en">

 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">

 	<title> Smart Desa | Kec. Benjeng Gresik</title>

 	<!--Favicon-->
 	<link rel="icon" href="{{asset('img/favicon.png')}}" type="image/jpg" />
 	<!-- Bootstrap CSS -->
 	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
 	<!-- Font Awesome CSS-->
 	<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
 	<!-- Line Awesome CSS -->
 	<link href="{{asset('css/line-awesome.min.css')}}" rel="stylesheet">
 	<!-- Animate CSS-->
 	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
 	<!-- Flaticon CSS -->
 	<link href="{{asset('css/flaticon.css')}}" rel="stylesheet">
 	<!-- Owl Carousel CSS -->
 	<link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
 	<!-- Style CSS -->
 	<link href="{{asset('css/style.css')}}" rel="stylesheet">
 	<!-- Responsive CSS -->
 	<link href="{{asset('css/responsive.css')}}" rel="stylesheet">
	 <link href="{{asset('css/new.css')}}" rel="stylesheet">
 	<!-- jquery -->
 	<script src="{{asset('js/jquery-1.12.4.min.js')}}"></script>

    <style>
    
    </style>
 </head>

 <body>

 	<!-- Pre-Loader -->
 	<div id="loader">
 		<div class="loading">
 			<div class="spinner">
 				<div class="double-bounce1"></div>
 				<div class="double-bounce2"></div>
 			</div>
 		</div>
 	</div>

 	<!-- Header Top Area -->
    <!-- Header Area -->

    <!-- TODO: CHANGE TO CSS FILE -->
    <!-- NORMAL NAV -->
    <header class="header-area" id="above-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-3">
 							<div class="logo">
 								<a class="navbar-brand" href="/">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng</span>
                                            </br>
											<span style="font-size: 12px;">Kab. Gresik | Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>

 						<div class="col-lg-9 col-md-6" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
 									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>
 									<div class="collapse navbar-collapse justify-content-center">
 										<ul class="navbar-nav m-auto">
 											<li class="nav-item">
 												<a class="nav-link active" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
                                                     <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                    <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 													
 												</ul>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 											</li>
 											<li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                     <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>
 											<li class="nav-item">
                                                <a class="nav-link" href="/pembuatan-kk">Pelayanan Publik</a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li style="padding-top: 20px">
                                                <a href="#" class="main-btn">Login</a>
                                            </li>
                                            <li style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
 						<div class="col-lg-2">
 							
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
    </header>
     <!-- END OF NORMAL NAV  -->
    
    <!-- NAV UNDER 1200 -->
     <header class="header-area" id="nav-under-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-6">
 							<div class="logo">
 								<a class="navbar-brand" href="/">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng <br> Kab. Gresik, Prov. Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>
						<div class="col-lg-6">
							<form method="post" action="index.html" id="mini-search-logo">
								<div class="form-group" style=display:flex;">
										<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
								</div>
							</form>
						</div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
                                    <button style="right: -1em" class="navbar-toggler search-btn" type="button" data-toggle="collapse">
                                         <i style="font-size: 30px;" class="la la-search"></i>
                                     </button>
 									<button style="right: 1em" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>

 									<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
 										<ul class="navbar-nav m-auto">
										 	<li class="nav-item" id="mini-search-burger">
											 <form method="post" action="index.html">
												<div class="form-group">
													<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
												</div>
												<hr>
											</form>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link active" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
                                                     <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                     <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 												</ul>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 											</li>
 											<li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                     <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>
 											<li class="nav-item">
                                                <a class="nav-link" href="/pembuatan-kk">Layanan Publik</a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn" id="login-1">Login</a>
                                                <a href="#" class="main-btn" id="login-2">login</a>
                                            </li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
                    </div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
     </header>
     <!-- END NAV UNDER 1200 -->

 	<!-- Hero Area -->

 	<div class="homepage-slides owl-carousel">
 		<div class="single-slide-item">
 			<div class="overlay"></div>
 			<div class="hero-area-content">
 				<div class="container">
 					<div class="row justify-content-center">
 						<div class="col-lg-12">
 							<div class="section-title">
                                 <h1>HUT Pemerintah <br>Kabupaten Gresik ke-44.
                                    <br>
                                    Dan Hari Jadi Kota Gresik ke-531
                                </h1>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="single-slide-item hero-area-bg-2">
 			<!-- <div class="overlay"></div> -->
 			<div class="hero-area-content">
 				<div class="container">
 					<div class="row justify-content-center">
 						<div class="col-lg-12 wow fadeInUp animated" data-wow-delay=".3s">
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- About Section-->

 	<div class="about-area section-padding">
 		<div class="container">
             <div class="section-title">
                 <h2>Statistik <b>COVID-19</b></h2>
             </div>
 			<div class="row">
 				<div class="col-lg-6 col-md-12 col-sm-12" style="margin-bottom: 30px">
 					<div class="info-content-area">
 						<div style="padding: 20px 30px; background-color:#3b5688; border-radius: 10px; box-shadow: 0px 15px 30px rgb(0 0 0 / 10%);">
                            <h6 style="text-align: center">
                                Indonesia
                            </h6>
                            <div class="achievement-area covid-19-report pt-0">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>Positif</h6>
                                            <p class="counter-number"><span>56</span></p>
                                            <h6>Jiwa</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>Sembuh</h6>
                                            <p class="counter-number"><span>46</span></p>
                                            <h6>Jiwa</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>Meninggal</h6>
                                            <p class="counter-number"><span>10</span></p>
                                            <h6>Jiwa</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="padding: 20px 30px; margin-top: 30px; background-color:#3b5688; border-radius: 10px; box-shadow: 0px 15px 30px rgb(0 0 0 / 10%);">
                            <h6 style="text-align: center">
                                Kab. Gresik
                            </h6>
                            <div class="achievement-area covid-19-report pt-0">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>Positif</h6>
                                            <p class="counter-number"><span>0</span></p>
                                            <h6>Jiwa</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>Positif</h6>
                                            <p class="counter-number"><span>0</span></p>
                                            <h6>Jiwa</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>Positif</h6>
                                            <p class="counter-number"><span>0</span></p>
                                            <h6>Jiwa</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
 					</div>
 				</div>

 				<div class="col-lg-6 col-md-12 col-sm-12">
                 <div style="padding: 20px 30px; background-color:#3b5688; border-radius: 10px; box-shadow: 0px 15px 30px rgb(0 0 0 / 10%);">
                            <h6 style="text-align: center">
                                Kecamatan Benjeng
                            </h6>
                            <div class="row" style="margin: 35px 30px 0 30px">
                                <div style="height: 300px; background-color: white; padding-top: 30px" class="col-lg-12">
									<canvas id="covid-chart" style="margin:auto;"></canvas>
                                </div>
                            </div>
                            <div class="achievement-area covid-19-report pt-0">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>OTG</h6>
                                            <p class="counter-number"><span>0</span></p>
                                            <h6>Kasus</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>ODP</h6>
                                            <p class="counter-number"><span>0</span></p>
                                            <h6>Kasus</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="single-counter-box">
                                            <h6>ODR</h6>
                                            <p class="counter-number"><span>0</span></p>
                                            <h6>Kasus</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
 				</div>

 			</div>
 		</div>
 	</div>

 	<!-- Articles Section -->

	 <div class="blog-area section-padding pb-10">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-6 col-md-12 col-12">
 					<div class="section-title">
 						<h2>Artikel <b>Terakhir</b></h2>
 					</div>
 				</div>

 				<div class="col-lg-6 col-md-6 text-right">
 					<a href="blog-classic.html" class="main-btn">Tampilkan Lebih Banyak</a>
 				</div>
 			</div>

 			<div class="row">
 				<div class="col-lg-6 col-md-12 col-12">
 					<div class="single-blog-item large">
 						<div class="blog-bg">
 							<img src="{{asset('img/blog/1.jpg')}}" alt="">
 						</div>
 						<div class="blog-content">
 							<div class="blog-meta">
 								<p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>25 Feb</p>
 							</div>
 							<a href="/detil-artikel">Pelantikan Bupati dan Wali Kota Terpilih di Jatim Ditunda.</a>
 							<p>Pelantikan sejumlah kepala daerah terpilih di Jawa Timur ditunda lantaran masih ada sejumlah sengketa Pilkada di Mahka...</p>
							<a href="/detil-artikel" style="font-size: 12px;">Selebihnya</a>
						</div>
 					</div>
 				</div>
 				<div class="col-lg-6 col-md-12 col-12">
 					<div class="single-blog-item small">
 						<div class="row">
 							<div class="col-lg-5 col-md-5 col-sm-12">
 								<div class="blog-bg">
 									<img src="{{asset('img/blog/2.jpg')}}" alt="">
 								</div>
 							</div>
 							<div class="col-lg-7 col-md-7 col-sm-12">
 								<div class="blog-content">
 									<div class="blog-meta">
 										<p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>17 Jan</p>
 									</div>
 									<a href="/detil-artikel">Harga Sejumlah Bahan Pokok Merangkak Naik.</a>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="single-blog-item small">
 						<div class="row">
 							<div class="col-lg-5 col-md-5 col-12">
 								<div class="blog-bg">
 									<img src="{{asset('img/blog/3.jpg')}}" alt="">
 								</div>
 							</div>
 							<div class="col-lg-7 col-md-7 col-12">
 								<div class="blog-content">
 									<div class="blog-meta">
 										<p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>01 Jan</p>
 									</div>
 									<a href="/detil-artikel">Lima Pembegal di Benjeng, Gresik Ditangkap Polisi.</a>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="single-blog-item small">
 						<div class="row">
 							<div class="col-lg-5 col-md-5 col-12">
 								<div class="blog-bg">
 									<img src="{{asset('img/blog/4.jpg')}}" alt="">
 								</div>
 							</div>
 							<div class="col-lg-7 col-md-7 col-12">
 								<div class="blog-content">
 									<div class="blog-meta">
 										<p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>24 Dec</p>
 									</div>
 									<a href="/detil-artikel">Bukit Jamur, Bukit Indah Bekas Pertambangan Kapur.</a>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Team Area -->

 	<div class="team_area gray-bg section-padding pb-150">
 		<div class="container">
 			<div class="row">
 				<div class="offset-lg-2 col-lg-8 text-center">
 					<div class="section-title">
 						<h2>Pemerintah <b>Desa</b></h2>
 						<p>Orang-Orang Beserta Jabatan</p>
 					</div>
 				</div>
 			</div>
 			<div class="row">
 				<div class="col-lg-3 col-md-6 col-sm-6 col-12">
 					<div class="single-team-member">
 						<div class="team-member-bg">
 							<div class="team-content">
 								<div class="team-title">
 									<a href="#">James Cameron</a>
 								</div>
 								<div class="team-subtitle">
 									<p>Kepala Desa</p>
 								</div>
 							</div>
 							<div class="team-social">
 								<ul>
 									<li>
 										<a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
 									</li>

 								</ul>
 							</div>
 						</div>

 					</div>
 				</div>

 				<div class="col-lg-3 col-md-6 col-sm-6 col-12">
 					<div class="single-team-member">
 						<div class="team-member-bg team-bg-2">
 							<div class="team-content">
 								<div class="team-title">
 									<a href="#">Mich Thomson</a>
 								</div>
 								<div class="team-subtitle">
 									<p>Sekretaris Desa</p>
 								</div>
 							</div>
 							<div class="team-social">
 								<ul>
 									<li>
 										<a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
 									</li>

 								</ul>
 							</div>
 						</div>
 					</div>
 				</div>

 				<div class="col-lg-3 col-md-6 col-sm-6 col-12">
 					<div class="single-team-member">
 						<div class="team-member-bg team-bg-3">
 							<div class="team-content">
 								<div class="team-title">
 									<a href="#">Josh Batlar</a>
 								</div>
 								<div class="team-subtitle">
 									<p>Kasi Pemerintahan</p>
 								</div>
 							</div>
 							<div class="team-social">
 								<ul>
 									<li>
 										<a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
 									</li>
 								</ul>
 							</div>
 						</div>
 					</div>
 				</div>

 				<div class="col-lg-3 col-md-6 col-sm-6 col-12">
 					<div class="single-team-member">
 						<div class="team-member-bg team-bg-4">
 							<div class="team-content">
 								<div class="team-title">
 									<a href="#">Simon Taffel</a>
 								</div>
 								<div class="team-subtitle">
 									<p>Kaur Keuangan</p>
 								</div>
 							</div>
 							<div class="team-social">
 								<ul>
 									<li>
 										<a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
 									</li>
 								</ul>
 							</div>
 						</div>
 					</div>
 				</div>

 				<div class="col-lg-3 col-md-6 col-sm-6 col-12">
 					<div class="single-team-member mt-120">
 						<div class="team-member-bg team-bg-5">
 							<div class="team-content">
 								<div class="team-title">
 									<a href="#">Albert Gill</a>
 								</div>
 								<div class="team-subtitle">
 									<p>Kepala Dusun 1</p>
 								</div>
 							</div>
 							<div class="team-social">
 								<ul>
 									<li>
 										<a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
 									</li>
 								</ul>
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class="col-lg-3 col-md-6 col-sm-6 col-12">
 					<div class="single-team-member mt-120">
 						<div class="team-member-bg team-bg-6">
 							<div class="team-content">
 								<div class="team-title">
 									<a href="#">Darek Wright</a>
 								</div>
 								<div class="team-subtitle">
 									<p>Kepala Dusun 2</p>
 								</div>
 							</div>
 							<div class="team-social">
 								<ul>
 									<li>
 										<a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
 									</li>
 								</ul>
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class="col-lg-3 col-md-6 col-sm-6 col-12">
 					<div class="single-team-member mt-120">
 						<div class="team-member-bg team-bg-7">
 							<div class="team-content">
 								<div class="team-title">
 									<a href="#">Jackob Onil</a>
 								</div>
 								<div class="team-subtitle">
 									<p>Kepala Dusun 3</p>
 								</div>
 							</div>
 							<div class="team-social">
 								<ul>
 									<li>
 										<a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
 									</li>
 								</ul>
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class="col-lg-3 col-md-6 col-sm-6 col-12">
 					<div class="single-team-member mt-120">
 						<div class="team-member-bg team-bg-8">
 							<div class="team-content">
 								<div class="team-title">
 									<a href="#">Richard Headley</a>
 								</div>
 								<div class="team-subtitle">
 									<p>Kepala Dusun 4</p>
 								</div>
 							</div>
 							<div class="team-social">
 								<ul>
 									<li>
 										<a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
 									</li>
 									<li>
 										<a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
 									</li>
 								</ul>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Programme Section -->

 	<div class="programe-area section-padding">
 		<div class="container">
 			<div class="row">
 				<div class="offset-lg-2 col-lg-8 text-center">
 					<div class="section-title">
 						<h2>Wilayah <b>Desa</b></h2>
 					</div>
 				</div>
 			</div>
 			<div class="programe-wrapper">
 				<div class="row no-gutters">
 					<div class="col-lg-12 col-md-12 col-12">
 						<div class="programe-inner">
						 	<!-- <button id="delete-button">Delete Selected Shape</button> -->
						 	<div id="map" style="height: 400px"></div>
							<div class="row" style="text-align: left">
								<div class="col-lg-6">
									<h5 style="color:#3b5688;">Lokasi Kantor Desa</h5>
									<p>Jl. Raya Munggugianti No.8, Bengkelolor, Bulurejo, Benjeng, Kabupaten Gresik, Jawa Timur 61172</p>
								</div>
								<div class="col-lg-6" style="text-align: right; padding-top: 60px">
									<a class="btn main-btn" href="https://www.google.com/maps/search/?api=1&query=-7.2631341,112.4983844" target="_blank">Buka Peta</a>
								</div>
							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>


 	<div class="spacer">
 		<div class="spacer-bg gray-bg bg-cover">
 		</div>
 	</div>

	<div class="gray-bg section-padding" style="padding-top: 0px">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-lg-12 col-md-12 col-12">
					<div class="programe-wrapper">
						<div class="row no-gutters">
							<div class="col-lg-12 col-md-12 col-12">
								<div class="programe-inner">
									<!-- <button id="delete-button">Delete Selected Shape</button> -->
									<div class="row" style="text-align: left">
										<div class="col-lg-6 float-right" style="text-align: right; padding-left: 30px; margin-bottom: 30px">
											<canvas id="chart-1" style="height: 300px; width: 100%"></canvas>
										</div>
										<div class="col-lg-6 col-md-12 col-12" style="padding-left: 30px">
											<div class="section-title">
												<h2>Statistik <b>Data Desa</b></h2>
												<p>Pilihan kategori data:</p>
												<div>
													<div class="form-check">
													<label class="form-check-label" for="exampleRadios1">
														<input class="form-check-input statistic-radio" type="radio" name="exampleRadios" id="exampleRadios1" value="gender" checked>
														Berdasarkan Jenis Kelamin
													</label>
													</div>
													<div class="form-check">
													<label class="form-check-label" for="exampleRadios2">
														<input class="form-check-input statistic-radio" type="radio" name="exampleRadios" id="exampleRadios2" value="religion">
														Berdasarkan Agama
													</label>
													</div>
													<div class="form-check disabled">
													<label class="form-check-label" for="exampleRadios3">
														<input class="form-check-input statistic-radio" type="radio" name="exampleRadios" id="exampleRadios3" value="job">
														Berdasarkan Pekerjaan
													</label>
													</div>
												</div>
												<div style="padding-top: 30px" >
													<a class="btn main-btn" href="/data-desa">Lebih Lanjut</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

 	<!-- Courses Section -->



 	<!-- Feature Section-->

 	<!--CTA Section-->


 	<!-- Event Section -->

 	<div class="event-area section-padding">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-10 offset-lg-1 text-center">
 					<div class="section-title">
						 <h2> <b>Acara </b>&<b> Kegiatan</b> Desa</h2>
 						<p>Daftar Acara dan kegiatan yang akan diselenggarakan oleh pihak desa atau Kecamatan</p>
 					</div>
 				</div>
 			</div>
 			<div class="row">
 				<div class="col-lg-6 col-md-6 col-12">
 					<div class="single-event-wrap">
 						<div class="row">
 							<div class="col-lg-5">
 								<div class="event-bg bg-cover">
 									<span class="event-date">11 Maret</span>
 								</div>
 							</div>
 							<div class="col-lg-7">
 								<div class="event-content">
 									<h6 style="color:#3b5688">Pembagian Sembako Memperingati Isra' Mi'raj</h6>
 									<p class="event-meta"> <i class="las la-clock"></i> <span>09.00 WIB </span> <br>
									<i class="las la-map-marker"></i> <span>Kantor kecamatan Benjeng</span> </p>
 									<p>Dalam memperingati Isra' Mi'raj Nabi Muhammad SAW Kantor ...</p>
 									<a href="single-event.html">Selebihnya</a>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class="col-lg-6 col-md-6 col-12">
 					<div class="single-event-wrap">
 						<div class="row">
 							<div class="col-lg-5">
 								<div class="event-bg bg-2 bg-cover">
 									<span class="event-date">20 Maret</span>
 								</div>
 							</div>
 							<div class="col-lg-7">
 								<div class="event-content">
 									<h6 style="color:#3b5688">Kerja Bakti Bersama di Kantor Kecamatan</h6>
									<p class="event-meta"> <i class="las la-clock"></i> <span>06.00 WIB </span>
									<br> <i class="las la-map-marker"></i>  <span>Kantor Kecamatan Benjeng</span> </p>
 									<p>Kerja Bakti Bulanan yang selalu dilakukan agar selalu menjaga...</p>
 									<a href="single-event.html">Selebihnya</a>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class="col-lg-6 col-md-6 col-12">
 					<div class="single-event-wrap">
 						<div class="row">
 							<div class="col-lg-5">
 								<div class="event-bg bg-3 bg-cover">
 									<span class="event-date">23 Maret</span>
 								</div>
 							</div>
 							<div class="col-lg-7">
 								<div class="event-content">
 									<h6 style="color:#3b5688">Vaksinisasi COVID-19</h6>
									<p class="event-meta"> <i class="las la-clock"></i> <span>07.00 WIB </span> 
									<br><i class="las la-map-marker"></i><span>Puskesmas Benjeng</span> </p>
 									<p>Akan diadakan pemberian vaksin COVID-19 secara massal... </p>
 									<a href="single-event.html">Selebihnya</a>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>

	 
	 

 	<!-- Testimonial Section  -->

 	<div class="testimonial-area sky-bg pt-50 pb-50">
 		<div class="container">
			 <div class="pb-4" style="text-align: center">
				 <h2 style="color:#3b5688;">Rincian <b>APBDes</b></h2>
			 </div>
 			<div class="row">
				<div class="col blue-bg" style="padding: 20px 30px;">
					<div class="row">
						<div class="col-lg-12 mb-2 mt-2">
							<h5 style="color: white" class="pb-2">APBDes Pelaksanaan</h5>
							<div class="row pb-2">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<h6 style="color: white">Pelaksanaan: Rp. <b>750.000</b></h6>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<h6 style="color: white">Anggaran: Rp. <b>15.000.000</b></h6>
								</div>
								<div class="col">
									<a href="" class="float-right"><i class="las la-download"></i> Download Detail</a>
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar" role="progressbar" style="width: 20%;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>
							</div>
						</div>
						<div class="col-lg-12 mb-2 mt-2">
							<h5 style="color: white" class="pb-2">APBDes Pendapatan</h5>
							<div class="row pb-2">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<h6 style="color: white">Pendapatan: Rp. <b>2.500.000</b></h6>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<h6 style="color: white">Anggaran: Rp. <b>5.000.000</b></h6>
								</div>
								<div class="col">
									<a href="" class="float-right"><i class="las la-download"></i> Download Detail</a>
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
							</div>
						</div>
						<div class="col-lg-12 mb-2 mt-2">
							<h5 style="color: white" class="pb-2">APBDes Pengeluaran</h5>
							<div class="row pb-2">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<h6 style="color: white">Pengeluaran: Rp. <b>750.000</b></h6>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<h6 style="color: white">Anggaran: Rp. <b>1.000.000</b></h6>
								</div>
								<div class="col">
									<a href="" class="float-right"><i class="las la-download"></i> Download Detail</a>
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar bg-warning" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">75%</div>
							</div>
						</div>
					</div>
				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Client Section  -->

 	<div class="client-area gray-bg section-padding">
 		<div class="container">
		 	<div class="row">
 				<div class="col-lg-10 offset-lg-1 text-center">
 					<div class="section-title">
						<h2> <b>Unggulan </b> Desa</h2>
 						<p>Unggulan desa mulai dari produk kuliner hingga tempat wisata</p>
 					</div>
 				</div>
 			</div>
 			<div class="row">
				<div class="owl-carousel unggulan-desa">
						<div style="padding-top: 15px; background-color: white" class="col-lg-10 col-md-12 col-sm-12 card">
								<div class="card-img-top" style="height: 200px; background-image:url({{asset('img/garam-1.jpg')}})"></div>
							<div class="card-body">
								<h5 class="card-title" style="color:#3b5688;">Garam Benjeng</h5>
							</div>
						</div>
						<div style="padding-top: 15px; background-color: white" class="col-lg-10 col-md-12 col-sm-12 card">
								<div style="height: 200px; background-image:url({{asset('img/wisata-1.jpeg')}})"></div>
							<div class="card-body">
								<h5 class="card-title"style="color:#3b5688"; >Wisata Gresik</h5>
							</div>
						</div>
						<div style="padding-top: 15px; background-color: white" class="col-lg-10 col-md-12 col-sm-12 card">
								<div style="height: 200px; background-image:url({{asset('img/budidaya.jpeg')}})"></div>
							<div class="card-body">
								<h5 class="card-title" style="color:#3b5688;">Budidaya Udang</h5>
							</div>
						</div>
						<div style="padding-top: 15px; background-color: white" class="col-lg-10 col-md-12 col-sm-12 card">
								<div style="height: 200px; background-image:url({{asset('img/kerupuk.jpg')}})"></div>
							<div class="card-body">
								<h5 class="card-title" style="color:#3b5688;">Kerupuk Ikan</h5>
							</div>
						</div>
				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Footer Area -->

 	<footer class="footer-area blue-bg">
 		<div class="container">
 			<div class="footer-up">
 				<div class="row">
 					<div class="col-lg-3 col-md-6 col-sm-12">
					 	<h5>Kecamatan Benjeng</h5>
 						<p>Jl. Raya Munggugianti No.8, Bengkelolor, Bulurejo, Benjeng, Kabupaten Gresik, Jawa Timur 61172</p>
 						<div class="social-area">
 							<a href=""><i class="lab la-facebook-f"></i></a>
 							<a href=""><i class="lab la-instagram"></i></a>
 							<a href=""><i class="lab la-twitter"></i></a>
 							<a href=""><i class="la la-skype"></i></a>
 						</div>
 					</div>
 					<div class="col-lg-2 offset-lg-1 col-md-6 com-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6 col-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6">
 						<div class="subscribe-form">
 							<h5>Berlangganan</h5>
 							<p>Dapatkan berita dan pengumuman terbaru</p>
 							<form action="index.html">
 								<input type="email" placeholder="Alamat Email">
 								<button class="main-btn">Mulai langganan</button>
 							</form>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</footer>

 	<!-- Footer Bottom Area -->
 	<div class="footer-bottom">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-6 col-md-6 col-sm-12">
 					<p class="copyright-line">© 2021 . All rights reserved.</p>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Scroll Top Area -->
 	<a href="#top" class="go-top"><i class="las la-angle-up"></i></a>


 	<!-- Popper JS -->
 	<script src="{{asset('js/popper.min.js')}}"></script>
 	<!-- Bootstrap JS -->
 	<script src="{{asset('js/bootstrap.min.js')}}"></script>
 	<!-- Wow JS -->
 	<script src="{{asset('js/wow.min.js')}}"></script>
 	<!-- Way Points JS -->
 	<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
 	<!-- Counter Up JS -->
 	<script src="{{asset('js/jquery.counterup.min.js')}}"></script>
 	<!-- CountDown JS -->
 	<script src="{{asset('js/jquery.countdown.js')}}"></script>
 	<!-- Owl Carousel JS -->
 	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
 	<!-- Sticky JS -->
	<script src="{{asset('js/jquery.sticky.js')}}"></script>
	<!-- CHART JS -->
	<script src="{{asset('js/chart.min.js')}}"></script>
 	<!-- Main JS -->
 	<script src="{{asset('js/main.js')}}"></script>
	 <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUCy2xwZBaACleFSfBwBehdJqTiyTe7VE&callback=initMap&libraries=&v=weekly&libraries=drawing"
      async
    ></script>

	<script>
		// NORMAL MAP
		let map;

		function initMap() {
			map = new google.maps.Map(document.getElementById("map"), {
				center: { lat: -7.2631341, lng: 112.4983844 },
				zoom: 12,
			});

			// add marker fo kantor camat
			new google.maps.Marker({
				position: { lat: -7.2631341, lng: 112.4983844 },
				map,
				title: "Kantor Camat Benjeng",
			});

			// add area
			const areaCoordinates = [
				{ lat: -7.2021802, lng: 112.5010101 },
				{ lat: -7.3, lng: 112.521501 },
				{ lat: -7.2821802, lng: 112.4620101 },
				{ lat: -7.216802, lng: 112.4310101 },
				{ lat: -7.2021802, lng: 112.5010101 }
			];
			// Construct the polygon.
			const area = new google.maps.Polygon({
				paths: areaCoordinates,
				strokeColor: "#FF0000",
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: "#FF0000",
				fillOpacity: 0.35,
			});

			area.setMap(map);
		}
	</script>
	<script>
	var covid = document.getElementById('covid-chart');
	var covidChart = new Chart(covid, {
		type: 'bar',
		data: {
			labels: ["ODP", "ODR", "OTG"],
			datasets: [{
				label: "Jumlah Kasus COVID-19 Desa",
				data: ["0", "0", "0"],
				backgroundColor: [
						'rgba(26, 56, 91, 0.267)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(52, 146, 28, 0.400)'
					]
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	})

	var ctx = document.getElementById('chart-1');
	var chartData = {};
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Laki-laki", "Perempuan", "total"],
			datasets: [{
				label: "Jumlah Penduduk Berdasarkan Jenis Kelamin",
				data: ["100", "130", "230"],
				backgroundColor: [
						'rgba(26, 56, 91, 0.267)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(52, 146, 28, 0.400)'
					]
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});
	function changeData(type = "gender") {
		switch (type) {
			case "gender":
				var labels = ["Laki-laki", "Perempuan", "total"];
				var data = {
					label: "Jumlah Penduduk Berdasarkan Jenis Kelamin",
					data: ["100", "130", "230"],
					backgroundColor: [
						'rgba(26, 56, 91, 0.267)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(52, 146, 28, 0.400)'
					]
				}
				myChart.data.labels = labels;
				myChart.data.datasets[0] = data;
				myChart.update();
				break;

			case "job":
				var labels = ["Bekerja", "Tidak Bekerja", "Belum Bekerja", "total"];
				var data = {
					label: "Jumlah Penduduk Berdasarkan Pekerjaan",
					data: ["106", "20", "104", "230"],
					backgroundColor: [
						'rgba(26, 56, 91, 0.267)',
						'rgba(255, 99, 132, 0.400)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(52, 146, 28, 0.400)',
					]
				}
				myChart.data.labels = labels;
				myChart.data.datasets[0] = data;
				myChart.update();
				break;
			case "religion":
			var labels = ["Islam", "Katolik", "Protestan", "Budha", "Hindu", "Kong Hu Cu", "Total"];
				var data = {
					label: "Jumlah Penduduk Berdasarkan Agama",
					data: ["180", "30", "20", "0", "0", "0", "230"],
					backgroundColor: [
						'rgba(26, 56, 91, 0.267)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(255, 99, 132, 0.400)',
						'rgba(56, 255, 49, 0.500)',
						'rgba(167, 43, 184, 0.500)',
						'rgba(49, 230, 214, 0.500)',
						'rgba(52, 146, 28, 0.400)',
						'rgba(52, 146, 28, 0.400)',
					]
				}
				myChart.data.labels = labels;
				myChart.data.datasets[0] = data;
				myChart.update();
				break;
		}
	}

	var radios = document.getElementsByClassName('statistic-radio');
	var prev = null;
	for (var i = 0; i < radios.length; i++) {
		radios[i].addEventListener('change', function() {
			(prev) ? console.log(prev.value): null;
			if (this !== prev) {
				prev = this;
				changeData(this.value)
			}
			console.log(this.value)
		});
	}
	</script>

	<script>
	$(document).ready(function(){
		$(".unggulan-desa").owlCarousel({
			items : 3, // THIS IS IMPORTANT
			// responsiveClass:true,
			responsive: {
				0:{
					responsiveRefreshRate: 1,
					items:1,
					nav:true
				},
				1000:{
					responsiveRefreshRate: 1,
					items:3,
					nav:false
				},
			}
		});
	});
	</script>
 </body>

 </html>
