<!DOCTYPE html>
 <html lang="en">

 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">

 	<title> Smart Desa | Kec. Benjeng Gresik</title>

 	<!--Favicon-->
 	<link rel="icon" href="{{asset('img/favicon.png')}}" type="image/jpg" />
 	<!-- Bootstrap CSS -->
 	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
 	<!-- Font Awesome CSS-->
 	<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
 	<!-- Line Awesome CSS -->
 	<link href="{{asset('css/line-awesome.min.css')}}" rel="stylesheet">
 	<!-- Animate CSS-->
 	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
 	<!-- Flaticon CSS -->
 	<link href="{{asset('css/flaticon.css')}}" rel="stylesheet">
 	<!-- Owl Carousel CSS -->
 	<link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
 	<!-- Style CSS -->
 	<link href="{{asset('css/style.css')}}" rel="stylesheet">
 	<!-- Responsive CSS -->
 	<link href="{{asset('css/responsive.css')}}" rel="stylesheet">
	 <link href="{{asset('css/new.css')}}" rel="stylesheet">
 	<!-- jquery -->
 	<script src="{{asset('js/jquery-1.12.4.min.js')}}"></script>

    <style>
    
    </style>
 </head>

 <body>

 	<!-- Pre-Loader -->
 	<div id="loader">
 		<div class="loading">
 			<div class="spinner">
 				<div class="double-bounce1"></div>
 				<div class="double-bounce2"></div>
 			</div>
 		</div>
 	</div>

 	<!-- Header Top Area -->
    <!-- Header Area -->

    <!-- TODO: CHANGE TO CSS FILE -->
    <!-- NORMAL NAV -->
    <header class="header-area" id="above-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-3">
 							<div class="logo">
 								<a class="navbar-brand" href="/">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng</span>
                                            </br>
                                            <span style="font-size: 12px">Kab. Gresik | Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>

 						<div class="col-lg-9 col-md-6" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
 									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>
 									<div class="collapse navbar-collapse justify-content-center">
 										<ul class="navbar-nav m-auto">
 											<li class="nav-item">
 												<a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
                                                     <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                    <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 													
 												</ul>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>

 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                     <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>

 											<li class="nav-item">
                                                <a class="nav-link active" href="/pembuatan-kk">Pelayanan Publik</a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn">Login</a>
                                            </li>
                                            <li style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
 						<div class="col-lg-2">
 							
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
    </header>
     <!-- END OF NORMAL NAV  -->
    
    <!-- NAV UNDER 1200 -->
     <header class="header-area" id="nav-under-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-12">
 							<div class="logo">
 								<a class="navbar-brand" href="/">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng <br> Kab. Gresik, Prov. Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
                                    <button style="right: -1em" class="navbar-toggler search-btn" type="button" data-toggle="collapse">
                                         <i style="font-size: 30px;" class="la la-search"></i>
                                     </button>
 									<button style="right: 1em" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>

 									<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
 										<ul class="navbar-nav m-auto">
 											<li class="nav-item">
 												<a class="nav-link" href="#">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
                                                     <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                     <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 													
 												</ul>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>

 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                     <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>

 											<li class="nav-item">
                                                <a class="nav-link active" href="/pembuatan-kk">Layanan Publik</a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn" id="login-1">Login</a>
                                                <a href="#" class="main-btn" id="login-2">login</a>
                                            </li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
                    </div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
     </header>
     <!-- END NAV UNDER 1200 -->

     <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Syarat Pembuatan KTP</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="mb-2"><i class="la la-circle"></i> &nbsp; Telah berusia 17 tahun</div>
                <div class="mb-2"><i class="la la-circle"></i> &nbsp; Fotokopi KK</div>
                <div class="mb-2"><i class="la la-circle"></i> &nbsp; Fotokopi Akta Kelahiran</div>
                <div class="mb-2"><i class="la la-circle"></i> &nbsp; Datang langsung ke kantor kecamatan untuk di-foto</div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

            </div>
        </div>
        </div>
     <div class="programe-area section-padding">
 		<div class="container">
 			<div class="row">
 				<div class="offset-lg-2 col-lg-8 text-center">
 					<div class="section-title">
                         <h2>Form Permintaan</h2>
                         <h2>Pembuatan <b>Kartu Tanda Penduduk (KTP)</b></h2>
 					</div>
 				</div>
 			</div>
 			<div class="programe-wrapper">
 				<div class="row no-gutters">
 					<div class="col-lg-12 col-md-12 col-12">
 						<div class="programe-inner">
                            <div class="alert alert-warning" role="alert">
                                Pastikan anda sudah memenuhi syarat-syarat pembuatan sebelum mengisi form.
                                <a data-toggle="modal" data-target="#myModal" style="color: #2388ff"><b>Lihat Syarat</b></a>
                            </div>

							<div class="row" style="text-align: left">
								<div class="col-lg-12">
                                <form>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label" for="form6Example1">Nomor Induk Kependudukan:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-outline">
                                            <input type="text" id="form6Example1" class="form-control" placeholder="NIK" />
                                        </div>
                                    </div>
                                </div>
                                <!-- 2 column grid layout with text inputs for the first and last names -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label" for="form6Example1">Nama Lengkap:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-outline">
                                            <input type="text" id="form6Example1" class="form-control" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label" for="form6Example1">Tempat / Tanggal Lahir:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-outline">
                                                    <input type="text" id="form6Example1" placeholder="Kota Kelahiran" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-outline">
                                                <input type="date" id="form6Example1" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label" for="form6Example1">Alamat Domisili:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-outline">
                                            <input type="text" id="form6Example3" class="form-control" />
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-3">
                                            <div class="form-outline">
                                                <input type="text" id="form6Example3" class="form-control" placeholder="RT"/>
                                            </div>
                                            </div>
                                            <div class="col-lg-2 col-md-3">
                                            <div class="form-outline">
                                                <input type="text" id="form6Example3" class="form-control" placeholder="RW"/>
                                            </div>
                                            </div>
                                            <div class="col-lg-8 col-md-6">
                                            <div class="form-outline">
                                                <input type="text" id="form6Example3" class="form-control" placeholder="Kelurahan"/>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label" for="form6Example3">Jenis Kelamin:</label>
                                    </div>
                                    <div class="col-md-9 mb-2">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-4">
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input statistic-radio" type="radio" name="exampleRadios" value="gender">
                                                        Laki-laki
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-4">
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input statistic-radio" type="radio" name="exampleRadios" value="gender">
                                                        Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Text input -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="sel1">Agama:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select class="form-control" id="sel1">
                                                <option value="" disabled selected>-- pilih salah satu --</option>
                                                <option>Budha</option>
                                                <option>Hindu</option>
                                                <option>Islam</option>
                                                <option>Katolik</option>
                                                <option>Kristen</option>
                                                <option>Kong Hu Cu</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="sel1">Jenis Pekerjaan</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select class="form-control" id="sel1">
                                                <option value="" disabled selected>-- pilih salah satu --</option>
                                                <option>Tidak/Belum Bekerja</option>
                                                <option>Mengurus Rumah Tangga.  </option>
                                                <option>Pelajar/ Mahasiswa</option>
                                                <option>Pensiunan</option>
                                                <option>Pewagai Negeri Sipil</option>
                                                <option>Tentara Nasional Indonesia</option>
                                                <option>Kepolisisan RI</option>
                                                <option>Perdagangan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form6Example3">Kewarga Negaraan</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9 mb-2">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-6">
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input statistic-radio" type="radio" name="wargaNegara" value="gender">
                                                        Warga Negara Indonesia
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6">
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input statistic-radio" type="radio" name="wargaNegara" value="gender">
                                                        Warga Negara Asing
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form6Example3">Upload KK</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-outline">
                                            <input type="file" id="form6Example1" class="form-control" />
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row justify-content-center mb-3">
                                    <div class="col-md-6" style="text-align: center">
                                        <h4>Data Tambahan</h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form6Example3">Alamat Email</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-outline">
                                            <input type="email" id="form6Example1" class="form-control" />
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form6Example3">No. Handphone</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-outline">
                                            <input type="text" id="form6Example1" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <!-- Email input -->
                                <!-- <div class="form-outline">
                                    <label class="form-label" for="form6Example5">Alamat Email</label>
                                    <input type="email" id="form6Example5" class="form-control" />
                                </div> -->

                                <!-- Number input -->
                                <!-- <div class="form-outline">
                                    <label class="form-label" for="form6Example6">No. Handphone</label>
                                    <input type="text" id="form6Example6" class="form-control" />
                                </div> -->

                                <!-- Checkbox -->
                                <!-- <div class="form-check d-flex justify-content-center">
                                    <input
                                    class="form-check-input me-2"
                                    type="checkbox"
                                    value=""
                                    id="form6Example8"
                                    checked
                                    />
                                    <label class="form-check-label" for="form6Example8"> Create an account? </label>
                                </div> -->

                                <!-- Submit button -->
                                <button type="submit" class="btn main-btn float-right">Kirim Permohonan</button>
                                </form>
								</div>
							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
     </div>
     
     <div class="spacer">
 		<div class="spacer-bg gray-bg bg-cover">
 		</div>
     </div>
     <div class="spacer">
 		<div class="spacer-bg gray-bg bg-cover">
 		</div>
 	</div>
 	<!-- Footer Area -->

 	<footer class="footer-area blue-bg">
 		<div class="container">
 			<div class="footer-up">
 				<div class="row">
 					<div class="col-lg-3 col-md-6 col-sm-12">
					 	<h5>Kecamatan Benjeng</h5>
 						<p>Jl. Raya Munggugianti No.8, Bengkelolor, Bulurejo, Benjeng, Kabupaten Gresik, Jawa Timur 61172</p>
 						<div class="social-area">
 							<a href=""><i class="lab la-facebook-f"></i></a>
 							<a href=""><i class="lab la-instagram"></i></a>
 							<a href=""><i class="lab la-twitter"></i></a>
 							<a href=""><i class="la la-skype"></i></a>
 						</div>
 					</div>
 					<div class="col-lg-2 offset-lg-1 col-md-6 com-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6 col-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6">
 						<div class="subscribe-form">
 							<h5>Berlangganan</h5>
 							<p>Dapatkan berita dan pengumuman terbaru</p>
 							<form action="index.html">
 								<input type="email" placeholder="Alamat Email">
 								<button class="main-btn">Mulai langganan</button>
 							</form>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</footer>

 	<!-- Footer Bottom Area -->
 	<div class="footer-bottom">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-6 col-md-6 col-sm-12">
 					<p class="copyright-line">© 2021 . All rights reserved.</p>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Scroll Top Area -->
 	<a href="#top" class="go-top"><i class="las la-angle-up"></i></a>


 	<!-- Popper JS -->
 	<script src="{{asset('js/popper.min.js')}}"></script>
 	<!-- Bootstrap JS -->
 	<script src="{{asset('js/bootstrap.min.js')}}"></script>
 	<!-- Wow JS -->
 	<script src="{{asset('js/wow.min.js')}}"></script>
 	<!-- Way Points JS -->
 	<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
 	<!-- Counter Up JS -->
 	<script src="{{asset('js/jquery.counterup.min.js')}}"></script>
 	<!-- CountDown JS -->
 	<script src="{{asset('js/jquery.countdown.js')}}"></script>
 	<!-- Owl Carousel JS -->
 	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
 	<!-- Sticky JS -->
	<script src="{{asset('js/jquery.sticky.js')}}"></script>
	<!-- CHART JS -->
	<script src="{{asset('js/chart.min.js')}}"></script>
 	<!-- Main JS -->
 	<script src="{{asset('js/main.js')}}"></script>
 </body>

 </html>
