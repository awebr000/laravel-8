<!DOCTYPE html>
 <html lang="en">

 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">

 	<title> Smart Desa | Kec. Benjeng Gresik</title>

 	<!--Favicon-->
 	<link rel="icon" href="{{asset('img/favicon.png')}}" type="image/jpg" />
 	<!-- Bootstrap CSS -->
 	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
 	<!-- Font Awesome CSS-->
 	<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
 	<!-- Line Awesome CSS -->
 	<link href="{{asset('css/line-awesome.min.css')}}" rel="stylesheet">
 	<!-- Animate CSS-->
 	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
 	<!-- Flaticon CSS -->
 	<link href="{{asset('css/flaticon.css')}}" rel="stylesheet">
 	<!-- Owl Carousel CSS -->
 	<link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
 	<!-- Style CSS -->
 	<link href="{{asset('css/style.css')}}" rel="stylesheet">
 	<!-- Responsive CSS -->
 	<link href="{{asset('css/responsive.css')}}" rel="stylesheet">
	 <link href="{{asset('css/new.css')}}" rel="stylesheet">
 	<!-- jquery -->
 	<script src="{{asset('js/jquery-1.12.4.min.js')}}"></script>

    <style>
    .bread-profil-desa:after{
        height: 100% !important;
    }
    .profil-desa h5 {
        color: #000;
    }
    .profil-desa h4 {
        color: #000;
    }
    .profil-desa h2 {
        color: #000;
    }
    </style>
 </head>

 <body>

 	<!-- Pre-Loader -->
 	<div id="loader">
 		<div class="loading">
 			<div class="spinner">
 				<div class="double-bounce1"></div>
 				<div class="double-bounce2"></div>
 			</div>
 		</div>
 	</div>

 	<!-- Header Top Area -->
    <!-- Header Area -->

    <!-- TODO: CHANGE TO CSS FILE -->
    <!-- NORMAL NAV -->
    <header class="header-area" id="above-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-3">
 							<div class="logo">
 								<a class="navbar-brand" href="/">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng</span>
                                            </br>
											<span style="font-size: 12px;">Kab. Gresik | Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>

 						<div class="col-lg-9 col-md-6" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
 									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>
 									<div class="collapse navbar-collapse justify-content-center">
 										<ul class="navbar-nav m-auto">
 											<li class="nav-item">
 												<a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link active" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
                                                     <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                    <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 													
 												</ul>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>

 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                     <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/pembuatan-kk">Pelayanan Publik</a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn">Login</a>
                                            </li>
                                            <li style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
 						<div class="col-lg-2">
 							
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
    </header>
     <!-- END OF NORMAL NAV  -->
    
    <!-- NAV UNDER 1200 -->
     <header class="header-area" id="nav-under-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-6">
 							<div class="logo">
 								<a class="navbar-brand" href="/">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng <br> Kab. Gresik, Prov. Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>
						<div class="col-lg-6">
							<form method="post" action="index.html" id="mini-search-logo">
								<div class="form-group" style=display:flex;">
										<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
								</div>
							</form>
						</div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
                                    <button style="right: -1em" class="navbar-toggler search-btn" type="button" data-toggle="collapse">
                                         <i style="font-size: 30px;" class="la la-search"></i>
                                     </button>
 									<button style="right: 1em" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>

 									<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
 										<ul class="navbar-nav m-auto">
										 	<li class="nav-item" id="mini-search-burger">
											 <form method="post" action="index.html">
												<div class="form-group">
													<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
												</div>
												<hr>
											</form>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link active" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
                                                     <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                     <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 													
 												</ul>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>

 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                     <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/pembuatan-kk">Layanan Publik</a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn" id="login-1">Login</a>
                                                <a href="#" class="main-btn" id="login-2">login</a>
                                            </li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
                    </div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
     </header>
     <!-- END NAV UNDER 1200 -->

     <div class="breadcroumb-area bread-profil-desa bread-bg">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-6 col-md-6">
 					<div class="breadcroumb-title">
 						<h1>Profil Desa</h1>
 						<h6><a href="/">Beranda</a> / Profil Desa</h6>
 					</div>
                </div>
                <div class="col-lg-6 col-md-6 d-none d-md-block pt-4">
                    <div style="height: 50px; margin-top: 15px; display: flex">
                        <div style="width: 70px; height: 70px;">
                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                        </div>
                        <div style="margin-left: 20px;white-space: normal">
                        <h4>
                            <span>Kec. Benjeng <br> Kab. Gresik, Prov. Jawa Timur</span>
                        </h4>
                        </div>
                    </div>
                </div>
 			</div>
 		</div>
 	</div>


     <div id="programme-page" class="programme-details-section section-padding profil-desa">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-4">
 					<div class="programme-list">
 						<h5></h5>
 						<a href="/profil-desa">Profil Wilayah Desa<span><i class="las la-arrow-right"></i></span></a>
 						<a href="/profil-desa/visi-misi">Visi dan Misi<span><i class="las la-arrow-right"></i></span></a>
 						<a class="active" href="/profil-desa/pemerintah-desa">Pemerintah Desa<span><i class="las la-arrow-right"></i></span></a>
 						
 					</div>

 					<!-- <div class="question-section">
 						<h6>Have any Question?</h6>
 						<form action="sendemail.php">
 							<input type="text" name="name" id="name" required="" placeholder="Full Name">
 							<input type="email" name="email" id="email" required="" placeholder="Your E-mail">
 							<textarea name="message" id="message" cols="30" rows="10" required="" placeholder="How can help you?"></textarea>
 							<button class="btn btn-primary" type="submit">Your Question</button>
 						</form>
 					</div> -->

 					<!-- <div class="helpline-section">
 						<div class="helpline-content text-center">
 							<h4>Need Consultancy Help</h4>
 							<p>Gatherin galso sprit moving shall flow</p>
 							<button class="btn btn-primary" type="submit">Contact Us</button>
 						</div>
 					</div> -->
 				</div>

 				<div class="col-lg-8">
 					<div class="single-programme">
                        <h2 style="color:#05205a;" class="mt-0">Pemerintah Desa</h2>
                        <h5 style="color:#3b5688;">Struktur Pemerintah dan Apartur Desa :</h5>
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="single-team-member">
                                    <div class="team-member-bg">
                                        <div class="team-content">
                                            <div class="team-title">
                                                <a href="#">James Cameron</a>
                                            </div>
                                            <div class="team-subtitle">
                                                <p>Kepala Desa</p>
                                            </div>
                                        </div>
                                        <div class="team-social">
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="single-team-member">
                                    <div class="team-member-bg team-bg-2">
                                        <div class="team-content">
                                            <div class="team-title">
                                                <a href="#">Mich Thomson</a>
                                            </div>
                                            <div class="team-subtitle">
                                                <p>Sekretaris Desa</p>
                                            </div>
                                        </div>
                                        <div class="team-social">
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="single-team-member">
                                    <div class="team-member-bg team-bg-3">
                                        <div class="team-content">
                                            <div class="team-title">
                                                <a href="#">Josh Batlar</a>
                                            </div>
                                            <div class="team-subtitle">
                                                <p>Kasi Pemerintahan</p>
                                            </div>
                                        </div>
                                        <div class="team-social">
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="single-team-member mt-120">
                                    <div class="team-member-bg team-bg-4">
                                        <div class="team-content">
                                            <div class="team-title">
                                                <a href="#">Simon Taffel</a>
                                            </div>
                                            <div class="team-subtitle">
                                                <p>Kaur Keuangan</p>
                                            </div>
                                        </div>
                                        <div class="team-social">
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="single-team-member mt-120">
                                    <div class="team-member-bg team-bg-5">
                                        <div class="team-content">
                                            <div class="team-title">
                                                <a href="#">Albert Gill</a>
                                            </div>
                                            <div class="team-subtitle">
                                                <p>Kepala Dusun 1</p>
                                            </div>
                                        </div>
                                        <div class="team-social">
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="single-team-member mt-120">
                                    <div class="team-member-bg team-bg-6">
                                        <div class="team-content">
                                            <div class="team-title">
                                                <a href="#">Darek Wright</a>
                                            </div>
                                            <div class="team-subtitle">
                                                <p>Kepala Dusun 2</p>
                                            </div>
                                        </div>
                                        <div class="team-social">
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="single-team-member mt-120">
                                    <div class="team-member-bg team-bg-7">
                                        <div class="team-content">
                                            <div class="team-title">
                                                <a href="#">Jackob Onil</a>
                                            </div>
                                            <div class="team-subtitle">
                                                <p>Kepala Dusun 3</p>
                                            </div>
                                        </div>
                                        <div class="team-social">
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="single-team-member mt-120">
                                    <div class="team-member-bg team-bg-8">
                                        <div class="team-content">
                                            <div class="team-title">
                                                <a href="#">Richard Headley</a>
                                            </div>
                                            <div class="team-subtitle">
                                                <p>Kepala Dusun 4</p>
                                            </div>
                                        </div>
                                        <div class="team-social">
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>


 	<footer class="footer-area blue-bg">
 		<div class="container">
 			<div class="footer-up">
 				<div class="row">
 					<div class="col-lg-3 col-md-6 col-sm-12">
					 	<h5>Kecamatan Benjeng</h5>
 						<p>Jl. Raya Munggugianti No.8, Bengkelolor, Bulurejo, Benjeng, Kabupaten Gresik, Jawa Timur 61172</p>
 						<div class="social-area">
 							<a href=""><i class="lab la-facebook-f"></i></a>
 							<a href=""><i class="lab la-instagram"></i></a>
 							<a href=""><i class="lab la-twitter"></i></a>
 							<a href=""><i class="la la-skype"></i></a>
 						</div>
 					</div>
 					<div class="col-lg-2 offset-lg-1 col-md-6 com-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6 col-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6">
 						<div class="subscribe-form">
 							<h5>Berlangganan</h5>
 							<p>Dapatkan berita dan pengumuman terbaru</p>
 							<form action="index.html">
 								<input type="email" placeholder="Alamat Email">
 								<button class="main-btn">Mulai langganan</button>
 							</form>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</footer>

 	<!-- Footer Bottom Area -->
 	<div class="footer-bottom">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-6 col-md-6 col-sm-12">
 					<p class="copyright-line">© 2021 . All rights reserved.</p>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Scroll Top Area -->
 	<a href="#top" class="go-top"><i class="las la-angle-up"></i></a>


 	<!-- Popper JS -->
 	<script src="{{asset('js/popper.min.js')}}"></script>
 	<!-- Bootstrap JS -->
 	<script src="{{asset('js/bootstrap.min.js')}}"></script>
 	<!-- Wow JS -->
 	<script src="{{asset('js/wow.min.js')}}"></script>
 	<!-- Way Points JS -->
 	<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
 	<!-- Counter Up JS -->
 	<script src="{{asset('js/jquery.counterup.min.js')}}"></script>
 	<!-- CountDown JS -->
 	<script src="{{asset('js/jquery.countdown.js')}}"></script>
 	<!-- Owl Carousel JS -->
 	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
 	<!-- Sticky JS -->
	<script src="{{asset('js/jquery.sticky.js')}}"></script>
	<!-- CHART JS -->
	<script src="{{asset('js/chart.min.js')}}"></script>
 	<!-- Main JS -->
 	<script src="{{asset('js/main.js')}}"></script>
	 <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUCy2xwZBaACleFSfBwBehdJqTiyTe7VE&callback=initMap&libraries=&v=weekly&libraries=drawing"
      async
    ></script>

	<script>
		// NORMAL MAP
		let map;

		function initMap() {
			map = new google.maps.Map(document.getElementById("map"), {
				center: { lat: -7.2631341, lng: 112.4983844 },
				zoom: 12,
			});

			// add marker fo kantor camat
			new google.maps.Marker({
				position: { lat: -7.2631341, lng: 112.4983844 },
				map,
				title: "Kantor Camat Benjeng",
			});

			// add area
			const areaCoordinates = [
				{ lat: -7.2021802, lng: 112.5010101 },
				{ lat: -7.3, lng: 112.521501 },
				{ lat: -7.2821802, lng: 112.4620101 },
				{ lat: -7.216802, lng: 112.4310101 },
				{ lat: -7.2021802, lng: 112.5010101 }
			];
			// Construct the polygon.
			const area = new google.maps.Polygon({
				paths: areaCoordinates,
				strokeColor: "#FF0000",
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: "#FF0000",
				fillOpacity: 0.35,
			});

			area.setMap(map);
		}
	</script>
 </body>

 </html>
