<!DOCTYPE html>
 <html lang="en">

 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">

 	<title> Smart Desa | Kec. Benjeng Gresik</title>

 	<!--Favicon-->
 	<link rel="icon" href="{{asset('img/favicon.png')}}" type="image/jpg" />
 	<!-- Bootstrap CSS -->
 	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
 	<!-- Font Awesome CSS-->
 	<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
 	<!-- Line Awesome CSS -->
 	<link href="{{asset('css/line-awesome.min.css')}}" rel="stylesheet">
 	<!-- Animate CSS-->
 	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
 	<!-- Flaticon CSS -->
 	<link href="{{asset('css/flaticon.css')}}" rel="stylesheet">
 	<!-- Owl Carousel CSS -->
 	<link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
 	<!-- Style CSS -->
 	<link href="{{asset('css/style.css')}}" rel="stylesheet">
 	<!-- Responsive CSS -->
 	<link href="{{asset('css/responsive.css')}}" rel="stylesheet">
	 <link href="{{asset('css/new.css')}}" rel="stylesheet">
 	<!-- jquery -->
 	<script src="{{asset('js/jquery-1.12.4.min.js')}}"></script>

    <style>
    
    </style>
 </head>

 <body>

 	<!-- Pre-Loader -->
 	<div id="loader">
 		<div class="loading">
 			<div class="spinner">
 				<div class="double-bounce1"></div>
 				<div class="double-bounce2"></div>
 			</div>
 		</div>
 	</div>

 	<!-- Header Top Area -->
    <!-- Header Area -->

    <!-- TODO: CHANGE TO CSS FILE -->
    <!-- NORMAL NAV -->
    <header class="header-area" id="above-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-3">
 							<div class="logo">
 								<a class="navbar-brand" href="index.html">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng</span>
                                            </br>
											<span style="font-size: 12px;">Kab. Gresik | Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>

 						<div class="col-lg-9 col-md-6" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
 									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>
 									<div class="collapse navbar-collapse justify-content-center">
 										<ul class="navbar-nav m-auto">
 											<li class="nav-item">
 												<a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
													<li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                    <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 												</ul>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>

 											</li>

 											<li class="nav-item">
                                                <a class="nav-link active" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                    <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/pembuatan-kk">Pelayanan Publik</a>
                                                <ul class="sub-menu">
													<li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn">Login</a>
                                            </li>
                                            <li style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
 						<div class="col-lg-2">
 							
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
    </header>
     <!-- END OF NORMAL NAV  -->
    
    <!-- NAV UNDER 1200 -->
     <header class="header-area" id="nav-under-1080">
 		<div class="sticky-area">
 			<div class="navigation">
 				<div class="container">
 					<div class="row">
 						<div class="col-lg-6">
 							<div class="logo">
 								<a class="navbar-brand" href="index.html">
                                     <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                        <p>
                                            <b>Website Resmi</b>
                                            </br>
                                            <span>Kec. Benjeng <br> Kab. Gresik, Prov. Jawa Timur</span>
                                        </p>
                                        </div>
                                    </div>
                                     <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
 							</div>
 						</div>
						<div class="col-lg-6">
							<form method="post" action="index.html" id="mini-search-logo">
								<div class="form-group" style=display:flex;">
										<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
								</div>
							</form>
						</div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12" style="margin-top: 15px">
 							<div class="main-menu">
 								<nav class="navbar navbar-expand-lg">
                                    <button style="right: -1em" class="navbar-toggler search-btn" type="button" data-toggle="collapse">
                                         <i style="font-size: 30px;" class="la la-search"></i>
                                     </button>
 									<button style="right: 1em" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 										<span class="navbar-toggler-icon"></span>
 									</button>

 									<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
 										<ul class="navbar-nav m-auto">
										 	<li class="nav-item" id="mini-search-burger">
											 <form method="post" action="index.html">
												<div class="form-group">
													<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
												</div>
												<hr>
											</form>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
 												</a>
 											</li>

 											<li class="nav-item">
 												<a class="nav-link" href="/profil-desa">Profil Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>
 												<ul class="sub-menu">
													<li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                    <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
 													<li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
 												</ul>
 											</li>
 											<li class="nav-item">
 												<a class="nav-link" href="/data-desa">Data Desa
 													<span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
 												</a>

 											</li>

 											<li class="nav-item">
                                                <a class="nav-link active" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                    <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
 												</ul>
 											</li>

 											<li class="nav-item">
                                                <a class="nav-link" href="/pembuatan-kk">Layanan Publik</a>
                                                <ul class="sub-menu">
													<li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
 												</ul>
 											</li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn" id="login-1">Login</a>
                                                <a href="#" class="main-btn" id="login-2">login</a>
                                            </li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
 										</ul>
                                     </div>
 								</nav>
 							</div>
 						</div>
                    </div>
 				</div>
 			</div>
 		</div>

 		<div class="search-popup">
 			<span class="search-back-drop"></span>

 			<div class="search-inner">
 				<div class="auto-container">
 					<!-- <div class="upper-text"> -->
 						<!-- <div class="text">Cari Artikel</div> -->
 						<!-- <button class="close-search"><span class="la la-times"></span></button> -->
 					<!-- </div> -->

 					<form method="post" action="index.html">
 						<div class="form-group">
 							<input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
 							<button type="submit"><i class="la la-search"></i></button>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
     </header>
     <!-- END NAV UNDER 1200 -->


     <div class="breadcroumb-area bread-profil-desa bread-bg">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-6 col-md-6">
 					<div class="breadcroumb-title">
 						<h1>Artikel</h1>
 						<h6><a href="/">Beranda</a> / Artikel</h6>
 					</div>
                </div>
                <div class="col-lg-6 col-md-6 d-none d-md-block pt-4">
                    <div style="height: 50px; margin-top: 15px; display: flex">
                        <div style="width: 70px; height: 70px;">
                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                        </div>
                        <div style="margin-left: 20px;white-space: normal">
                        <h4>
                            <span>Kec. Benjeng <br> Kab. Gresik, Prov. Jawa Timur</span>
                        </h4>
                        </div>
                    </div>
                </div>
 			</div>
 		</div>
 	</div>


	 <div class="blog-area section-padding pb-0">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-4 col-md-6 col-sm-12">
 					<div class="single-blog-item medium">
 						<div class="blog-bg">
 							<img src="{{asset('img/blog/1.jpg')}}" alt="">
 						</div>
 						<div class="blog-content">
 							<p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>25 Feb</p>
 							<h5 style="color: red;"><a href="/detil-artikel">Pelantikan Bupati dan Wali Kota Terpilih di Jatim Ditunda.</a></h5>
 							<p>Pelantikan sejumlah kepala daerah terpilih di Jawa Timur ditunda lantaran masih ada sejumlah sengketa Pilkada ...</p>
 							<a href="/detil-artikel" class="read-more">Selanjutnya</a>
 						</div>
 					</div>
 				</div>
 				<div class="col-lg-4 col-md-6 col-sm-12">
 					<div class="single-blog-item medium">
 						<div class="blog-bg">
 							<img src="{{asset('img/blog/2.jpg')}}" alt="">
 						</div>
 						<div class="blog-content">
 							<p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>16 Jan</p>
 							<h5><a href="/detil-artikel">Harga Sejumlah Bahan Pokok Merangkak Naik</a></h5>
 							<p>Berdasarkan pantauan di Pusat Informasi Harga pangan Strategis (PIHPS), pada Senin (8/6), harga rata...</p>
 							<a href="/detil-artikel" class="read-more">Selanjutnya</a>
 						</div>
 					</div>
 				</div>
 				<div class="col-lg-4 col-md-6 col-sm-12">
 					<div class="single-blog-item medium">
 						<div class="blog-bg">
 							<img src="{{asset('img/blog/3.jpg')}}" alt="">
 						</div>
 						<div class="blog-content">
 							<p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>01 Jan</p>
 							<a href="/detil-artikel">Lima Pembegal Di Benjeng, Gresik Ditangkap Polisi.</a>
 							<h5><p>Lima tersangka yang ditangkap yakni S, AS, EU, MA, dan TT. Dari lima tersangka, dua tersangka yaitu TT dan AS ditem...</p></h5>
 							<a href="/detil-artikel" class="read-more">Selanjutnya</a>
 						</div>
 					</div>
 				</div>
 				<div class="pagination-block mb-15">
 					<a class="page-numbers" href="blog-grid.html">1</a>
 					<a class="page-numbers current" href="blog-grid.html">2</a>
 					<a class="next page-numbers" href="blog-grid.html"><i class="las la-arrow-right"></i></a>
 				</div>
 			</div>
 		</div>
 	</div>


 	<footer class="footer-area blue-bg">
 		<div class="container">
 			<div class="footer-up">
 				<div class="row">
 					<div class="col-lg-3 col-md-6 col-sm-12">
					 	<h5>Kecamatan Benjeng</h5>
 						<p>Jl. Raya Munggugianti No.8, Bengkelolor, Bulurejo, Benjeng, Kabupaten Gresik, Jawa Timur 61172</p>
 						<div class="social-area">
 							<a href=""><i class="lab la-facebook-f"></i></a>
 							<a href=""><i class="lab la-instagram"></i></a>
 							<a href=""><i class="lab la-twitter"></i></a>
 							<a href=""><i class="la la-skype"></i></a>
 						</div>
 					</div>
 					<div class="col-lg-2 offset-lg-1 col-md-6 com-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6 col-sm-12">
 						
 					</div>
 					<div class="col-lg-3 col-md-6">
 						<div class="subscribe-form">
 							<h5>Berlangganan</h5>
 							<p>Dapatkan berita dan pengumuman terbaru</p>
 							<form action="index.html">
 								<input type="email" placeholder="Alamat Email">
 								<button class="main-btn">Mulai langganan</button>
 							</form>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</footer>

 	<!-- Footer Bottom Area -->
 	<div class="footer-bottom">
 		<div class="container">
 			<div class="row">
 				<div class="col-lg-6 col-md-6 col-sm-12">
 					<p class="copyright-line">© 2021 . All rights reserved.</p>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Scroll Top Area -->
 	<a href="#top" class="go-top"><i class="las la-angle-up"></i></a>


 	<!-- Popper JS -->
 	<script src="{{asset('js/popper.min.js')}}"></script>
 	<!-- Bootstrap JS -->
 	<script src="{{asset('js/bootstrap.min.js')}}"></script>
 	<!-- Wow JS -->
 	<script src="{{asset('js/wow.min.js')}}"></script>
 	<!-- Way Points JS -->
 	<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
 	<!-- Counter Up JS -->
 	<script src="{{asset('js/jquery.counterup.min.js')}}"></script>
 	<!-- CountDown JS -->
 	<script src="{{asset('js/jquery.countdown.js')}}"></script>
 	<!-- Owl Carousel JS -->
 	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
 	<!-- Sticky JS -->
	<script src="{{asset('js/jquery.sticky.js')}}"></script>
	<!-- CHART JS -->
	<script src="{{asset('js/chart.min.js')}}"></script>
 	<!-- Main JS -->
 	<script src="{{asset('js/main.js')}}"></script>
	 <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUCy2xwZBaACleFSfBwBehdJqTiyTe7VE&callback=initMap&libraries=&v=weekly&libraries=drawing"
      async
    ></script>

	<script>
		// NORMAL MAP
		let map;

		function initMap() {
			map = new google.maps.Map(document.getElementById("map"), {
				center: { lat: -7.2631341, lng: 112.4983844 },
				zoom: 12,
			});

			// add marker fo kantor camat
			new google.maps.Marker({
				position: { lat: -7.2631341, lng: 112.4983844 },
				map,
				title: "Kantor Camat Benjeng",
			});

			// add area
			const areaCoordinates = [
				{ lat: -7.2021802, lng: 112.5010101 },
				{ lat: -7.3, lng: 112.521501 },
				{ lat: -7.2821802, lng: 112.4620101 },
				{ lat: -7.216802, lng: 112.4310101 },
				{ lat: -7.2021802, lng: 112.5010101 }
			];
			// Construct the polygon.
			const area = new google.maps.Polygon({
				paths: areaCoordinates,
				strokeColor: "#FF0000",
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: "#FF0000",
				fillOpacity: 0.35,
			});

			area.setMap(map);
		}
	</script>
	<script>
	var covid = document.getElementById('covid-chart');
	var covidChart = new Chart(covid, {
		type: 'bar',
		data: {
			labels: ["ODP", "ODR", "OTG"],
			datasets: [{
				label: "Jumlah Kasus COVID-19 Desa",
				data: ["0", "0", "0"],
				backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(52, 146, 28, 0.658)'
					]
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	})

	var ctx = document.getElementById('chart-1');
	var chartData = {};
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Laki-laki", "Perempuan", "total"],
			datasets: [{
				label: "Jumlah Penduduk Berdasarkan Jenis Kelamin",
				data: ["100", "130", "230"],
				backgroundColor: [
						'rgba(26, 56, 91, 0.267)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(52, 146, 28, 0.400)'
					]
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});
	function changeData(type = "gender") {
		switch (type) {
			case "gender":
				var labels = ["Laki-laki", "Perempuan", "total"];
				var data = {
					label: "Jumlah Penduduk Berdasarkan Jenis Kelamin",
					data: ["100", "130", "230"],
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(209, 162, 43, 0.500)',
						'rgba(52, 146, 28, 0.400)'
					]
				}
				myChart.data.labels = labels;
				myChart.data.datasets[0] = data;
				myChart.update();
				break;

			case "job":
				var labels = ["Bekerja", "Tidak Bekerja", "Belum Bekerja", "total"];
				var data = {
					label: "Jumlah Penduduk Berdasarkan Pekerjaan",
					data: ["106", "20", "104", "230"],
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(26, 56, 91, 0.267)',
						'rgba(52, 146, 28, 0.400)',
						'rgba(26, 56, 91, 0.267)',
					]
				}
				myChart.data.labels = labels;
				myChart.data.datasets[0] = data;
				myChart.update();
				break;
			case "religion":
			var labels = ["Islam", "Katolik", "Protestan", "Budha", "Hindu", "Kong Hu Cu", "Total"];
				var data = {
					label: "Jumlah Penduduk Berdasarkan Agama",
					data: ["180", "30", "20", "0", "0", "0", "230"],
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(26, 56, 91, 0.267)',
						'rgba(52, 146, 28, 0.400)',
						'rgba(26, 56, 91, 0.267)',
						'rgba(52, 146, 28, 0.400)',
						'rgba(255, 99, 132, 0.2)',
						'rgba(26, 56, 91, 0.267)',
						'rgba(52, 146, 28, 0.400)',
					]
				}
				myChart.data.labels = labels;
				myChart.data.datasets[0] = data;
				myChart.update();
				break;
		}
	}

	var radios = document.getElementsByClassName('statistic-radio');
	var prev = null;
	for (var i = 0; i < radios.length; i++) {
		radios[i].addEventListener('change', function() {
			(prev) ? console.log(prev.value): null;
			if (this !== prev) {
				prev = this;
				changeData(this.value)
			}
			console.log(this.value)
		});
	}
	</script>

	<script>
	$(document).ready(function(){
		$(".unggulan-desa").owlCarousel({
			items : 3, // THIS IS IMPORTANT
			// responsiveClass:true,
			responsive: {
				0:{
					responsiveRefreshRate: 1,
					items:1,
					nav:true
				},
				1000:{
					responsiveRefreshRate: 1,
					items:3,
					nav:false
				},
			}
		});
	});
	</script>
 </body>

 </html>
