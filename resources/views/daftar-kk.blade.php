<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pembuatan Kartu Keluarga</title>

    <!--Favicon-->
    <link rel="icon" href="{{asset('img/favicon.png')}}" type="image/jpg" />
    <!-- Bootstrap CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome CSS-->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Line Awesome CSS -->
    <link href="{{asset('css/line-awesome.min.css')}}" rel="stylesheet">
    <!-- Animate CSS-->
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <!-- Flaticon CSS -->
    <link href="{{asset('css/flaticon.css')}}" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('css/new.css')}}" rel="stylesheet">
    <!-- jquery -->
    <script src="{{asset('js/jquery-1.12.4.min.js')}}"></script>

    <style>

    </style>
</head>

<body>

    <!-- Pre-Loader -->
    <div id="loader">
        <div class="loading">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>

    <!-- Header Top Area -->
    <!-- Header Area -->

    <!-- TODO: CHANGE TO CSS FILE -->
    <!-- NORMAL NAV -->
    <header class="header-area" id="above-1080">
        <div class="sticky-area">
            <div class="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="logo">
                                <a class="navbar-brand" href="/">
                                    <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                            <p>
                                                <b>Website Resmi</b>
                                                </br>
                                                <span>Kec. Benjeng</span>
                                                </br>
                                                <span style="font-size: 12px">Kab. Gresik | Jawa Timur</span>
                                            </p>
                                        </div>
                                    </div>
                                    <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-6" style="margin-top: 15px">
                            <div class="main-menu">
                                <nav class="navbar navbar-expand-lg">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <span class="navbar-toggler-icon"></span>
                                        <span class="navbar-toggler-icon"></span>
                                        <span class="navbar-toggler-icon"></span>
                                    </button>
                                    <div class="collapse navbar-collapse justify-content-center">
                                        <ul class="navbar-nav m-auto">
                                            <li class="nav-item">
                                                <a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
                                                </a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" href="/profil-desa">Profil Desa
                                                    <span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                    <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
                                                    <li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
                                                    
                                                </ul>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="/data-desa">Data Desa
                                                    <span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
                                                </a>

                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" href="/pengumuman">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pengumuman">Pengumuman</a></li>
                                                    <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link active" href="/pembuatan-kk">Pelayanan Publik</a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
                                                </ul>
                                            </li>
                                            <li style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn">Login</a>
                                            </li>
                                            <li style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="col-lg-2">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="search-popup">
            <span class="search-back-drop"></span>

            <div class="search-inner">
                <div class="auto-container">
                    <!-- <div class="upper-text"> -->
                    <!-- <div class="text">Cari Artikel</div> -->
                    <!-- <button class="close-search"><span class="la la-times"></span></button> -->
                    <!-- </div> -->

                    <form method="post" action="index.html">
                        <div class="form-group">
                            <input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
                            <button type="submit"><i class="la la-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
    <!-- END OF NORMAL NAV  -->

    <!-- NAV UNDER 1200 -->
    <header class="header-area" id="nav-under-1080">
        <div class="sticky-area">
            <div class="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="logo">
                                <a class="navbar-brand" href="/">
                                    <div style="height: 50px; margin-top: 15px; display: flex">
                                        <div style="width: 70px; height: 70px;">
                                            <img style="width: 100%; margin: 0 !important" src="{{asset('img/client/1.png')}}" alt="">
                                        </div>
                                        <div style="margin-left: 20px;white-space: normal">
                                            <p>
                                                <b>Website Resmi</b>
                                                </br>
                                                <span>Kec. Benjeng <br> Kab. Gresik, Prov. Jawa Timur</span>
                                            </p>
                                        </div>
                                    </div>
                                    <!-- <img src="{{asset('img/logo.png')}}" alt=""> -->
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="margin-top: 15px">
                            <div class="main-menu">
                                <nav class="navbar navbar-expand-lg">
                                    <button style="right: -1em" class="navbar-toggler search-btn" type="button" data-toggle="collapse">
                                        <i style="font-size: 30px;" class="la la-search"></i>
                                    </button>
                                    <button style="right: 1em" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <span class="navbar-toggler-icon"></span>
                                        <span class="navbar-toggler-icon"></span>
                                        <span class="navbar-toggler-icon"></span>
                                    </button>

                                    <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                                        <ul class="navbar-nav m-auto">
                                            <li class="nav-item">
                                                <a class="nav-link" href="/">Beranda
                                                    <span class="sub-nav-toggler"></span>
                                                    <i class=""></i>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="/profil-desa">Profil Desa
                                                    <span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li><a href="/profil-desa">Profil Wilayah Desa</a></li>
                                                    <li><a href="/profil-desa/visi-misi">Visi dan Misi</a></li>
                                                    <li><a href="/profil-desa/pemerintah-desa">Pemerintah Desa</a></li>
                                                </ul>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="/data-desa">Data Desa
                                                    <span class="sub-nav-toggler">
                                                    </span>
                                                    <i class=""></i>
                                                </a>

                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" href="/acara">Berita Desa
                                                    <i class=""></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- <li><a href="/pengumuman">Pengumuman</a></li> -->
                                                    <li><a href="/acara">Acara Desa</a></li>
                                                    <li><a href="/artikel">Artikel Desa</a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link active" href="/pembuatan-kk">Layanan Publik</a>
                                                <ul class="sub-menu">
                                                    <li><a href="/pembuatan-kk">Pembuatan KK</a></li>
                                                    <li><a href="/pembuatan-akta">Pembuatan Akta Kelahiran</a></li>
                                                    <li><a href="/pembuatan-ktp">Pembuatan KTP</a></li>
                                                    <li><a href="/permintaan-skck">Permintaan SKCK</a></li>
                                                    <li><a href="/perijinan-umkm">Perijinan UMKM</a></li>
                                                    <li><a href="/lapor-keluhan">Lapor Keluhan</a></li>
                                                </ul>
                                            </li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <a href="courses.html" class="main-btn" id="login-1">Login</a>
                                                <a href="#" class="main-btn" id="login-2">login</a>
                                            </li>
                                            <li class="nav-item" style="padding-top: 20px">
                                                <div class="search-box" style="padding: 13px 0;margin-left: 25px;">
                                                    <button class="search-btn" style="position: static"><i style="font-size: 30px;" class="la la-search"></i></button>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="search-popup">
            <span class="search-back-drop"></span>

            <div class="search-inner">
                <div class="auto-container">
                    <!-- <div class="upper-text"> -->
                    <!-- <div class="text">Cari Artikel</div> -->
                    <!-- <button class="close-search"><span class="la la-times"></span></button> -->
                    <!-- </div> -->

                    <form method="post" action="index.html">
                        <div class="form-group">
                            <input type="search" name="search-field" value="" placeholder="Cari Artikel..." required="">
                            <button type="submit"><i class="la la-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
    <!-- END NAV UNDER 1200 -->

    <!-- Hero Area -->

    <!-- About Section-->

    <!-- Articles Section -->

    <!-- Programme Section -->

    <div class="programe-area section-padding">
        <div class="container">
            <div class="row">
                <div class="offset-lg-2 col-lg-8 text-center">
                    <div class="section-title">
                        <h2>Form Permintaan</h2>
                        <h2>Pembuatan <b>Kartu Keluarga (KK) </b></h2>
                    </div>
                </div>
            </div>
            <div class="programe-wrapper">
                <div class="row no-gutters">
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="programe-inner">
                            <div class="alert alert-warning" role="alert">
                                Pastikan anda telah memenuhi syarat dan ketentuan sebelum mengisi
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Formulir Biodata Pemohon
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row" style="text-align: left;">
                                                <div class="col-lg-12">
                                                    <form>
                                                        <div class="row ml-2">
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">NIK (<i>Nomor Induk Kependudukan</i>) : </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Nama Lengkap: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Tempat/Tanggal Lahir: </label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-outline">
                                                                    <input type="date" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Tempat/Tanggal Lahir: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <select class="form-control" id="sel1">
                                                                        <option value="" disabled selected>-- pilih salah satu --</option>
                                                                        <option>Budha</option>
                                                                        <option>Hindu</option>
                                                                        <option>Islam</option>
                                                                        <option>Katolik</option>
                                                                        <option>Kristen</option>
                                                                        <option>Kong Hu Cu</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label class="form-label" for="form1">Jenis Kelamin: </label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-check-inline">
                                                                    <label class="form-check-label">
                                                                        <input class="form-check-input statistic-radio" type="radio" name="exampleRadios" value="gender">
                                                                        Laki-laki
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-check-inline">
                                                                    <label class="form-check-label">
                                                                        <input class="form-check-input statistic-radio" type="radio" name="exampleRadios" value="gender">
                                                                        Perempuan
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Alamat: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4"></div>
                                                            <div class="col-md-1">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" placeholder="RT" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" placeholder="RW" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" placeholder="Desa" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4"></div>
                                                            <div class="col-md-3">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" placeholder="Kecamatan" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" placeholder="Kabupaten" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Kewarganegaraan: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Pekerjaan: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <select class="form-control" id="sel1">
                                                                        <option value="" disabled selected>-- pilih salah satu --</option>
                                                                        <option>Pegawai Negeri</option>
                                                                        <option>Belum Bekerja</option>
                                                                        <option>Mahasiswa</option>
                                                                        <option>Pegawai Swasta</option>
                                                                        <option>Ibu Rumah Tangga</option>
                                                                        <option>Wiraswasta</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Status: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <select class="form-control" id="sel1">
                                                                        <option value="" disabled selected>-- pilih salah satu --</option>
                                                                        <option>Belum Menikah</option>
                                                                        <option>Menikah</option>
                                                                        <option>Duda</option>
                                                                        <option>Janda</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">No. Hp: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Email: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn main-btn float-right">Konfirmasi</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                Syarat Permohonan Kartu Keluarga Baru
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row" style="text-align: left;">
                                                <div class="col-lg-12">
                                                    <form>
                                                        <div class="row ml-2">
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">No Kartu Keluarga: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Foto Buku Nikah: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Surat Pengantar dari Desa: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Surat Keterangan Pindah : </label>
                                                                <label for="form-label"><i style="font-size: 10px;">(Khusus untuk Pendatang)</i></label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn main-btn float-right">Konfirmasi</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                                Syarat Penambahan Anggota Keluarga
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row" style="text-align: left;">
                                                <div class="col-lg-12">
                                                    <form>
                                                        <div class="row ml-2">
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">No Kartu Keluarga: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Foto Kartu Keluarga Lama: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Surat Pengantar dari Desa: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Surat Keterangan Kelahiran Anggota Keluarga yang akan ditambahkan: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn main-btn float-right">Konfirmasi</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                                Syarat Pengurangan Anggota Keluarga
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row" style="text-align: left;">
                                                <div class="col-lg-12">
                                                    <form>
                                                        <div class="row ml-2">
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">No Kartu Keluarga: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="text" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Foto Kartu Keluarga Lama: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Surat Pengantar dari Desa: </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Surat Keterangan Kematian: </label>
                                                                <label for="form label"><i style="font-size: 10px;">(Khusus untuk anggota keluarga yang meninggal)</i></label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mt-2">
                                                                <label class="form-label" for="form1">Upload Surat Keterangan Pindah: </label>
                                                                <label for="form label"><i style="font-size: 10px;">(Khusus untuk anggota keluarga yang pindah)</i></label>
                                                            </div>
                                                            <div class=" col-md-6">
                                                                <div class="form-outline">
                                                                    <input type="file" id="form1" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn main-btn float-right">Konfirmasi</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Courses Section -->



    <!-- Feature Section-->

    <!--CTA Section-->


    <!-- Event Section -->


    <!-- Testimonial Section  -->

    <!-- Blog Section -->

    <!-- Client Section  -->

    <!-- Footer Area -->


    <!-- Footer Bottom Area -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <p class="copyright-line">© 2021 . All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Scroll Top Area -->
    <a href="#top" class="go-top"><i class="las la-angle-up"></i></a>


    <!-- Popper JS -->
    <script src="{{asset('js/popper.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Wow JS -->
    <script src="{{asset('js/wow.min.js')}}"></script>
    <!-- Way Points JS -->
    <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
    <!-- Counter Up JS -->
    <script src="{{asset('js/jquery.counterup.min.js')}}"></script>
    <!-- CountDown JS -->
    <script src="{{asset('js/jquery.countdown.js')}}"></script>
    <!-- Owl Carousel JS -->
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <!-- Sticky JS -->
    <script src="{{asset('js/jquery.sticky.js')}}"></script>
    <!-- CHART JS -->
    <script src="{{asset('js/chart.min.js')}}"></script>
    <!-- Main JS -->
    <script src="{{asset('js/main.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUCy2xwZBaACleFSfBwBehdJqTiyTe7VE&callback=initMap&libraries=&v=weekly&libraries=drawing" async></script>

    <script>
        // NORMAL MAP
        let map;

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                center: {
                    lat: -7.2631341,
                    lng: 112.4983844
                },
                zoom: 12,
            });

            // add marker fo kantor camat
            new google.maps.Marker({
                position: {
                    lat: -7.2631341,
                    lng: 112.4983844
                },
                map,
                title: "Kantor Camat Benjeng",
            });

            // add area
            const areaCoordinates = [{
                    lat: -7.2021802,
                    lng: 112.5010101
                },
                {
                    lat: -7.3,
                    lng: 112.521501
                },
                {
                    lat: -7.2821802,
                    lng: 112.4620101
                },
                {
                    lat: -7.216802,
                    lng: 112.4310101
                },
                {
                    lat: -7.2021802,
                    lng: 112.5010101
                }
            ];
            // Construct the polygon.
            const area = new google.maps.Polygon({
                paths: areaCoordinates,
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0.35,
            });

            area.setMap(map);
        }
    </script>

    <script>
        var covid = document.getElementById('covid-chart');
        var covidChart = new Chart(covid, {
            type: 'bar',
            data: {
                labels: ["ODP", "ODR", "OTG"],
                datasets: [{
                    label: "Jumlah Kasus COVID-19 Desa",
                    data: ["0", "0", "0"],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)'
                    ]
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        })

        var ctx = document.getElementById('chart-1');
        var chartData = {};
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Laki-laki", "Perempuan", "total"],
                datasets: [{
                    label: "Jumlah Penduduk Berdasarkan Jenis Kelamin",
                    data: ["100", "130", "230"],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)'
                    ]
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        function changeData(type = "gender") {
            switch (type) {
                case "gender":
                    var labels = ["Laki-laki", "Perempuan", "total"];
                    var data = {
                        label: "Jumlah Penduduk Berdasarkan Jenis Kelamin",
                        data: ["100", "130", "230"],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ]
                    }
                    myChart.data.labels = labels;
                    myChart.data.datasets[0] = data;
                    myChart.update();
                    break;

                case "job":
                    var labels = ["Bekerja", "Tidak Bekerja", "Belum Bekerja", "total"];
                    var data = {
                        label: "Jumlah Penduduk Berdasarkan Pekerjaan",
                        data: ["106", "20", "104", "230"],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                        ]
                    }
                    myChart.data.labels = labels;
                    myChart.data.datasets[0] = data;
                    myChart.update();
                    break;
                case "religion":
                    var labels = ["Islam", "Katolik", "Protestan", "Budha", "Hindu", "Kong Hu Cu", "Total"];
                    var data = {
                        label: "Jumlah Penduduk Berdasarkan Agama",
                        data: ["180", "30", "20", "0", "0", "0", "230"],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                        ]
                    }
                    myChart.data.labels = labels;
                    myChart.data.datasets[0] = data;
                    myChart.update();
                    break;
            }
        }

        var radios = document.getElementsByClassName('statistic-radio');
        var prev = null;
        for (var i = 0; i < radios.length; i++) {
            radios[i].addEventListener('change', function() {
                (prev) ? console.log(prev.value): null;
                if (this !== prev) {
                    prev = this;
                    changeData(this.value)
                }
                console.log(this.value)
            });
        }
    </script>

    <script>
        $(document).ready(function() {
            $(".unggulan-desa").owlCarousel({
                items: 3, // THIS IS IMPORTANT
                // responsiveClass:true,
                responsive: {
                    0: {
                        responsiveRefreshRate: 1,
                        items: 1,
                        nav: true
                    },
                    1000: {
                        responsiveRefreshRate: 1,
                        items: 3,
                        nav: false
                    },
                }
            });
        });
    </script>
</body>

</html>