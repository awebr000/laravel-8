<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pembuatan-kk', function () {
    return view('daftar-kk');
});

Route::get('/pembuatan-ktp', function () {
    return view('pembuatan-ktp');
});

Route::get('/data-desa', function () {
    return view('data-desa');
});

Route::get('detil-artikel', function () {
    return view('detil-artikel');
});

Route::get('artikel', function () {
    return view('artikel');
});

Route::get('acara', function () {
    return view('acara');
});

Route::get('detil-acara', function () {
    return view('detil-acara');
});


Route::prefix('profil-desa')->group(function () {
    Route::get('/', function () {
        return view('profil-desa/main');
    });
    Route::get('/visi-misi', function () {
        return view('profil-desa/visi-misi');
    });
    Route::get('/pemerintah-desa', function () {
        return view('profil-desa/pemerintah');
    });
});